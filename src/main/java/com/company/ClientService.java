package com.company;

import entities.Clients;
import entities.DVD;

import java.util.List;

public class ClientService {
    private ClientsDAO clientsDAO = new ClientsDAO();
    public ClientService(){}
    public Clients findClient(int id) { return clientsDAO.findById(id);}
    public void saveClient(Clients client) { clientsDAO.save(client); }
    public void deleteClient(int clientId) { clientsDAO.delete(clientId);}
    public void updateClient(String fn, String sn, int tel, int clientId) { clientsDAO.update(fn, sn, tel, clientId);}
    public void updateClient(Clients client) { clientsDAO.update(client);}
    public List<Clients> findAllClients() { return clientsDAO.findAllClients(); }
    public DVD findDVD(int id) { return clientsDAO.findDVDById(id);}
    public void deleteDVD(int id) { clientsDAO.deleteDVD(id);}
    public List<DVD> findAllDVD() {return clientsDAO.findAllDVD();}
    public List<DVD> findDVDbyClient(int clientId){ return clientsDAO.findDVDbyClient(clientId);}
    public void updateDVD(DVD dvd) { clientsDAO.update(dvd); }
}
