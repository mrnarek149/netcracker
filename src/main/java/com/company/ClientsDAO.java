package com.company;

import entities.DVD;
import org.hibernate.Session;
import org.hibernate.Transaction;
import entities.Clients;

import java.util.List;

public class ClientsDAO {

    public Clients findById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Clients client = session.get(Clients.class, id);
        session.close();
        return client;
    }

    public void save(Clients client) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(client);
        transaction.commit();
        session.close();
    }

    public void update(String fn, String sn, int tel, int clientId) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.get(Clients.class, clientId).setFirstName(fn);
        session.get(Clients.class, clientId).setSecondName(sn);
        session.get(Clients.class, clientId).setTelephone(tel);
        transaction.commit();
        session.close();
    }
    public void update(Clients client) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(client);
        transaction.commit();
        session.close();
    }
    public void update(DVD dvd) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(dvd);
        transaction.commit();
        session.close();
    }

    public void delete(int clientId) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Clients clients = session.get(Clients.class, clientId);
        session.delete(clients);
        transaction.commit();
        session.close();
    }

    public DVD findDVDById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        DVD dvd = session.get(DVD.class, id);
        session.close();
        return dvd;
    }

    public List<Clients> findAllClients() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Clients> client = (List<Clients>) session.createQuery("FROM Clients").list();
        session.close();
        return client;
    }

    public void deleteDVD(int id){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        DVD dvd = session.get(DVD.class, id);
        session.delete(dvd);
        transaction.commit();
        session.close();
    }

    public List<DVD> findAllDVD(){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<DVD> dvd = (List<DVD>) session.createQuery("FROM DVD").list();
        session.close();
        return dvd;
    }

    public List<DVD> findDVDbyClient(int clientsId){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<DVD> dvds = (List<DVD>) session.createQuery("FROM DVD WHERE client.clientId = " + clientsId).list();
        session.close();
        return dvds;
    }
}
