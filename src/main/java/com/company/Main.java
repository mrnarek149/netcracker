package com.company;

import entities.Clients;
import entities.DVD;
import jsonxml.JsonParser;
import jsonxml.Parser;
import jsonxml.XMLParser;

import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        ClientService clientService = new ClientService();
        Scanner scanner = new Scanner(System.in);
        int choice = -1;
        while(choice != 0) {
            System.out.println("1) Добавить клиента");
            System.out.println("2) Изменить данные о клиенте");
            System.out.println("3) Удалить клиента (удалятся и связанные с ним DVD)");
            System.out.println("4) Добавить DVD к клиенту");
            System.out.println("5) Изменить DVD");
            System.out.println("6) Удалить DVD");
            System.out.println("7) Вывод всех DVD");
            System.out.println("8) Вывод клиентов");
            System.out.println("9) Вывод клиентов с прикрепленными DVD");
            System.out.println("10) Загрузка клиента");
            System.out.println("11) Выгрузка клиента");
            System.out.println("0) Выход");
            choice = scanner.nextInt();
            switch (choice){
                case 1:{// Добавить клиента
                    System.out.println("Введите имя, фамилию и телефон клиента:");
                    String firstName = scanner.next();
                    String secondName = scanner.next();

                    int tel = scanner.nextInt();
                    Clients clients = new Clients(firstName, secondName, tel);
                    clientService.saveClient(clients);
                }
                    break;
                case 2: {// Изменить клиента
                    List<Clients> clients = clientService.findAllClients();
                    for(Clients client: clients){
                        System.out.println(client.toString());
                    }
                    System.out.print("Введите Id клиента: ");
                    int client_id = scanner.nextInt();
                    System.out.println("Что хотите изменить?\n1) Имя\n2) Фамилию\n3) Телефон\n4) Всё");
                    int key = scanner.nextInt();
                    switch (key){
                        case 1:{
                            System.out.print("Введите имя: ");
                            String firstName = scanner.next();
                            Clients client =  clientService.findClient(client_id);
                            client.setFirstName(firstName);
                            clientService.updateClient(client);
                            break;
                        }
                        case 2:{
                            System.out.print("Введите фамилию: ");
                            String secondName = scanner.next();
                            Clients client =  clientService.findClient(client_id);
                            client.setSecondName(secondName);
                            clientService.updateClient(client);
                            break;
                        }
                        case 3:{
                            System.out.print("Введите телефон: ");
                            int tel = scanner.nextInt();
                            Clients client =  clientService.findClient(client_id);
                            client.setTelephone(tel);
                            clientService.updateClient(client);
                            break;
                        }
                        case 4:{
                            System.out.println("Введите имя, фамилию и телефон клиента:");
                            String first_name = scanner.next();
                            String secondName = scanner.next();
                            int tel = scanner.nextInt();
                            clientService.updateClient(first_name, secondName, tel, client_id);
                            break;
                        }
                        default:{
                            System.out.println("Такого значения нет");
                            break;
                        }
                    }
                }
                    break;
                case 3:{// Удалить клиента
                    List<Clients> clients = clientService.findAllClients();
                    for(Clients client: clients){
                        System.out.println(client.toString());
                    }
                    System.out.println("Введите Id клиента: ");
                    int client_id = scanner.nextInt();
                    clientService.deleteClient(client_id);
                }
                    break;
                case 4:{// DVD добавить
                    System.out.println("Введите русское название, английское название, год выхода:");
                    String rn = scanner.next();
                    String en = scanner.next();
                    int year = scanner.nextInt();

                    DVD dvd = new DVD(rn, en, year);
                    List<Clients> clients = clientService.findAllClients();
                    for(Clients client: clients){
                        System.out.println(client.toString());
                    }
                    System.out.println("Введите Id клиента, которому выдали этот DVD");
                    int clientId = scanner.nextInt();

                    Clients client = clientService.findClient(clientId);
                    client.addDVD(dvd);
                    clientService.updateClient(client);
                }
                    break;
                case 5:{// DVD изменить
                    List<DVD> dvds = clientService.findAllDVD();
                    for(DVD dvd: dvds){
                        System.out.println(dvd.toString());
                    }
                    System.out.println("Введите Id DVD");
                    int dvdId = scanner.nextInt();

                    System.out.println("Что хотите изменить?\n1) Русское название\n2) Английское название\n3) Год выхода\n4) Всё");
                    int key = scanner.nextInt();
                    switch (key){
                        case 1:{
                            System.out.print("Введите русское название: ");
                            String rn = scanner.next();
                            DVD dvd = clientService.findDVD(dvdId);
                            dvd.setRussianName(rn);
                            clientService.updateDVD(dvd);
                            break;
                        }
                        case 2:{
                            System.out.print("Введите английское название: ");
                            String en = scanner.next();
                            DVD dvd = clientService.findDVD(dvdId);
                            dvd.setEnglishName(en);
                            clientService.updateDVD(dvd);
                            break;
                        }
                        case 3:{
                            System.out.print("Введите год выхода: ");
                            int year = scanner.nextInt();
                            DVD dvd = clientService.findDVD(dvdId);
                            dvd.setYear(year);
                            clientService.updateDVD(dvd);
                            break;
                        }
                        case 4:{
                            System.out.println("Введите русское название, английское название, год выхода:");
                            String rn = scanner.next();
                            String en = scanner.next();
                            int year = scanner.nextInt();
                            DVD dvd = clientService.findDVD(dvdId);
                            dvd.setRussianName(rn);
                            dvd.setEnglishName(en);
                            dvd.setYear(year);
                            clientService.updateDVD(dvd);
                        }
                        default:{
                            System.out.println("Такого значения нет");
                            break;
                        }
                    }
                }
                    break;
                case 6:{// DVD удалить
                    List<DVD> dvds = clientService.findAllDVD();
                    for(DVD dvd: dvds){
                        System.out.println(dvd.toString());
                    }
                    System.out.println("Введите Id DVD");
                    int dvdId = scanner.nextInt();
                    clientService.deleteDVD(dvdId);
                }
                    break;
                case 7:{// Вывод DVD
                    List<DVD> dvds = clientService.findAllDVD();
                    for(DVD dvd: dvds){
                        System.out.println(dvd.toString());
                    }
                }
                    break;
                case 8: {// Вывод клиентов
                    List<Clients> clients = clientService.findAllClients();
                    for (Clients client : clients
                    ) {
                        System.out.println(client.toString());
                    }
                }
                    break;
                case 9:{// Вывод клиентов с DVD
                    List<Clients> clients = clientService.findAllClients();
                    for(Clients client: clients){
                        System.out.println(client.toString());
                        List<DVD> dvds = clientService.findDVDbyClient(client.getClientId());
                        if(dvds != null) for(DVD dvd: dvds) System.out.println(dvd.toString());
                    }break;
                }
                case 10:{
                    Parser parser = null;
                    System.out.println("1)JSON \n2)XML");
                    int type;
                    do {
                        type = scanner.nextInt();
                    }while (type != 1 && type != 2);
                        switch (type) {
                            case 1: {
                                parser = new JsonParser();
                                Clients clients = parser.createClient();
                                clientService.saveClient(clients);
                                break;
                            }
                            case 2: {
                                parser = new XMLParser();
                                Clients clients = parser.createClient();
                                clientService.saveClient(clients);
                                break;
                            }
                        }
                    break;
                }
                case 11:{
                    List<Clients> clients = clientService.findAllClients();
                    for (Clients client : clients
                    ) {
                        System.out.println(client.toString());
                    }
                    System.out.println("Укажите id: "); int id = scanner.nextInt();
                    Clients client = clientService.findClient(id);
                    Parser parser;
                    System.out.println("1)JSON \n2)XML");
                    int type;
                    do {
                        type = scanner.nextInt();
                    } while (type != 1 && type != 2);
                        switch (type) {
                            case 1: {
                                parser = new JsonParser();
                                parser.unloadClient(client);
                                break;
                            }
                            case 2: {
                                parser = new XMLParser();
                                parser.unloadClient(client);
                                break;
                            }
                    }
                    break;
                }
                case 0:
                    break;
                default:
                    System.out.println("Неверный выбор. Повторите попытку.");
                    break;
            }
        }
    }
}
