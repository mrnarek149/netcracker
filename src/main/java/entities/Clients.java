package entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "clients")
public @Getter @Setter class Clients{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int clientId;

    @Column
    private String firstName;
    @Column
    private String secondName;
    private int telephone;

    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<DVD> dvdList;

    public Clients(){}
    public Clients(String fn, String sn, int tel){
        firstName = fn;
        secondName = sn;
        telephone = tel;
        dvdList = new ArrayList<DVD>();
    }
    public void addDVD(DVD dvd){
        dvd.setClient(this);
        dvdList.add(dvd);
    }
    public void removeDVD(DVD dvd){ dvdList.remove(dvd); }

    @Override
    public String toString() {
        return "Client " + clientId + ": " + secondName + " " + firstName + " " + telephone;
    }
}
