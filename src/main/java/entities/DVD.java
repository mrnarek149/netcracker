package entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "dvd_disc")
public @Getter @Setter class DVD{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String russianName;
    @Column
    private String englishName;
    private int year;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "clientId")
    private Clients client;

    public DVD() {}
    public DVD(String rn, String en, int year){
        russianName = rn;
        englishName = en;
        this.year = year;
    }

    @Override
    public String toString() {
        return "DVD " + id + ": " + russianName + " " + englishName + " " + year;
    }
}
