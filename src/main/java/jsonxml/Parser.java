package jsonxml;

import entities.Clients;

public interface Parser {
    Clients createClient();
    void unloadClient(Clients clients);
}
