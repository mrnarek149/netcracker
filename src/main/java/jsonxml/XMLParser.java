package jsonxml;

import entities.Clients;
import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class XMLParser implements Parser {
    public Clients createClient() {
        Clients clients = null;
        try {
            int tel; String firstName, secondName;
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse("XML.xml");
            doc.getDocumentElement().normalize();
            System.out.println("Root Element: " + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("Clients");
            for(int i = 0; i < nList.getLength(); i++){
                Node nNode = nList.item(i);
                if(nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element) nNode;
                    firstName = eElement.getElementsByTagName("firstName").item(0).getTextContent();
                    secondName = eElement.getElementsByTagName("secondName").item(0).getTextContent();
                    tel = Integer.parseInt(eElement.getElementsByTagName("tel").item(0).getTextContent());
                    clients = new Clients(firstName, secondName, tel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clients;
    }

    public void unloadClient(Clients clients) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();

            Element rootElement = doc.createElement("Clients");
            doc.appendChild(rootElement);

            Element clientId = doc.createElement("clientId");
            clientId.appendChild(doc.createTextNode(String.valueOf(clients.getClientId())));
            rootElement.appendChild(clientId);

            Element firstName = doc.createElement("firstName");
            firstName.appendChild(doc.createTextNode(clients.getFirstName()));
            rootElement.appendChild(firstName);

            Element secondName = doc.createElement("secondName");
            secondName.appendChild(doc.createTextNode(clients.getSecondName()));
            rootElement.appendChild(secondName);

            Element tel = doc.createElement("tel");
            tel.appendChild(doc.createTextNode(String.valueOf(clients.getTelephone())));
            rootElement.appendChild(tel);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("XML.xml"));
            transformer.transform(source, result);
        }catch (Exception e){e.printStackTrace(); }
    }
}
