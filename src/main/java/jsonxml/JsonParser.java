package jsonxml;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import entities.*;
import jdk.nashorn.internal.parser.JSONParser;

import java.io.*;

public class JsonParser implements Parser {

    public Clients createClient() {
        Clients clients = null;
        try {
            StringBuilder jsonString = new StringBuilder();
            BufferedReader br = new BufferedReader(new FileReader(new File("Json.json")));
            jsonString.append(br.readLine());
            System.out.println(jsonString);
            clients = new ObjectMapper().readValue(jsonString.toString(), Clients.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return clients;
    }

    public void unloadClient(Clients clients) {
        File file = new File("Json.json");
        try {
            FileWriter dos = new FileWriter(file);
            Clients clients1 = new Clients();
            clients1.setClientId(clients.getClientId());
            clients1.setTelephone(clients.getTelephone());
            clients1.setFirstName(clients.getFirstName());
            clients1.setSecondName(clients.getSecondName());
            String jsonString = new ObjectMapper().writeValueAsString(clients1);
            dos.write(jsonString);
            dos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
